#
# read .bashrc
#
if [ -f ~/.bashrc ] ; then
. ~/.bashrc
fi

#
# PATH
#
export PATH=/usr/local/bin:$PATH
if [ -d $HOME/.local ]; then  
        export PATH=$HOME/.local/bin:$PATH
fi
if [ -d $HOME/.yarn ]; then
        export PATH=$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH
fi
if [ -d $HOME/.local/src/sratoolkit.2.11.2-ubuntu64 ]; then
        export PATH=$HOME/.local/src/sratoolkit.2.11.2-ubuntu64/bin:$PATH
fi
if [ -d $HOME/.local/lib/pkgconfig ]; then
        export PKG_CONFIG_PATH=$HOME/.local/lib/pkgconfig:$PKG_CONFIG_PATH
fi
if [ -d $HOME/.local/src/nvim-linux64 ]; then
        export PATH=$HOME/.local/src/nvim-linux64/bin:$PATH
fi

#
# MODULE
#
if ! command -v module &> /dev/null
then
    echo "module command could not be found"
    return
else
    module use /big/mrk/app/.modulefiles
    module load last/1542
fi

#
# To use GenomeTools on python
#
#if [ -d $HOME/.local/src/genometools-1.6.2 ]; then
#        export PYTHONPATH=$PYTHONPATH:$HOME/.local/src/genometools-1.6.2/gtpython
#        export LD_LIBRARY_PATH=$HOME/.local/src/genometools-1.6.2/lib
#fi

# To use Nextflow, set java version to 17
# execute only on my MacOS laptop
if [ "$(uname)" == 'Darwin' ]; then
        export JAVA_HOME=$(/usr/libexec/java_home -v 17)
fi

# 
# ls Colors
#
export CLICOLOR=1
export LSCOLORS=gxfxcxdxbxegedabagacad

#
# alias
#
alias ..='cd ..'
alias less='less -N -S'
#alias vi='nvim'
#alias v='nvim'
alias targz='tar -zxvf'
#alias tar_='tar -xvf'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'

# Hide user name and machine name
# execute only on biohazard
if [ "$(expr substr $(uname -s) 1 5)" == 'Linux' ]; then
        export PS1="\w $ "
fi
# export PS1="\w ~ "

# For now, execute only on biohazard
#if [ "$(expr substr $(uname -s) 1 5)" == 'Linux' ]; then
#        eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`
#        export MANPATH=$HOME/perl5/man:$MANPATH
#        eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`
#        export MANPATH=$HOME/perl5/man:$MANPATH
#fi


test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"


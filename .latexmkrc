#!/usr/bin/env perl
#@default_files    = ('main.tex');
$latex            = 'platex -synctex=1 -halt-on-error';
$latex_silent     = 'platex -synctex=1 -halt-on-error -interaction=batchmode';
$max_repeat       = 5;
$ENV{'TZ'}        = 'Asia/Tokyo'; # Time Zone

# BibTex
$bibtex           = 'pbibtex %O %B';
$biber            = 'biber --bblencoding=utf8 -u -U --output_safechars';

# index
$makeindex        = 'mendex %O -o %D %S';

# PDF
$pdf_mode         = 3; # tex -> dvi -> pdf
# $pdf_mode         = 1; # tex -> pdf 
# $pdflatex         = 'pdflatex -synctex=1 -halt-on-error -interaction=nonstopmode';
# @generated_exts   = (@generated_exts, 'synctex.gz');
$dvipdf           = 'dvipdfmx %O -o %D %S';


# preview
# -------------------------------------------------------------------
# latexmk -pv hoge.tex: show PDF after build
# latexmk -pvc hoge.tex: automatically update PDF everytime you save
# -------------------------------------------------------------------
#$pvc_view_file_via_temporary = 0;
#if ($^O eq 'linux') {
#    $dvi_previewer = "xdg-open %S";
#    $pdf_previewer = "xdg-open %S";
#} elsif ($^O eq 'darwin') {
#    $dvi_previewer = "open %S";
#    $pdf_previewer = "open %S";
#} else {
#    $dvi_previewer = "start %S";
#    $pdf_previewer = "start %S";
#}
$pdf_previewer    = "open -ga /Applications/Skim.app";

# clean up
#$clean_full_ext = "%R.synctex.gz"

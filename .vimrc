"""""""""""""""""
" Vundle PlugIn
"""""""""""""""""
set nocompatible              " be iMproved, required
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()           " required
" required
Plugin 'VundleVim/Vundle.vim' 
" ================
" my plugins here
" ================
Plugin 'nvie/vim-flake8'
" A static syntax and style checker for Python:
" it is a wrapper around PyFlakes(static syntax checker)
" PEP8(style checker) 
" and Ned's MacCabe script (complexity checker)
Plugin 'dense-analysis/ale'
" Check syntax in Vim asynchronously and fix files, 
" with Language Server Protocol (LSP) support:
" it provides linting (syntax checking and semantic 
" errors) while you edit your text files, and acts as
" a Vim Language Server Protocol client.
Plugin 'preservim/nerdtree'
" A file system explorer for Vim:
" you can visually browse complex directory hierarchies,
" quickly open files for reading or editing, and 
" perform basic file system operations.
Plugin 'ctrlpvim/ctrlp.vim'
" Fuzzy file, buffer, mru, tag etc finder:
" - built-in Most Recently Used (MRU) files monitoring.
" - built-in project's root finder.
" - open multiple files at once.
" - create new files and directories.
Plugin 'altercation/vim-colors-solarized' 
" Precision colorscheme for Vim.
Plugin 'cohama/lexima.vim'
" Auto close parentheses and repeat by dot dot dot...
" basically, you can automatically close pairs such as
" (), {}, ... But in advance, you can also customize
" the rule to automatically input any character on any
" context.
Plugin 'kana/vim-submode'
" Create your own submodes
Plugin 'tpope/vim-surround'
" Provides mappings to easily delete, change and add
" surroundings such as (), {}, etc. in pairs.
" Press cs"' inside \"Hello world!\" to change it to
" 'Hello world!'
Plugin 'tpope/vim-dispatch'
" Leverage the power of Vim's compiler plugins without 
" being bound by synchronicity. Kick off builds and test
" suites using ine of several asynchronous adapters
" (including tmux, screen, iTerm, Windows, and a 
" headless mode), and when the job completes, errors 
" will be loaded and parsed automatically.
Plugin 'tmhedberg/SimpylFold'
" Simple, correct folding for Python:
" it properly folds class and function/method 
" definitions, and leaves your loops and conditional 
" blocks untouched.
Plugin 'vim-scripts/indentpython.vim'
" An alternative indentation script for Python:
" it handles continuation lines implied by open 
" (parentheses), [brackets] and {braces} correctly and 
" it indents multiline if/for/while statements 
" differently.
Plugin 'OmniSharp/omnisharp-vim'
" Provides IDE like abilities for C#.
" it relies on the OmniSharp-Rosllyn server, a .NET
" development platform used by several editors 
" including Visual Studio Code, Emacs, Atom and others.
Plugin 'neoclide/coc.nvim'
" Make your Vim as smart as VSCode
Plugin 'LukeGoodsell/nextflow-vim'
" syntax highlighting of .nf scripts. 
Plugin 'lervag/vimtex'
" A modern Vim filetype and syntax plugin for LaTeX files.
" <Space>ll: Start (or stop) compiling the document
" <Space>lk: Stop the compilation process
" <Space>lc: Clear auxiliary files
" <Space>lv: Foward search. This will open the compiled PDF.
" Some PDF viewers support the ability to jump to the current location in the
" PDF. Some also support backward search (jumping from PDF to source).
" <Space>le: VimTex will populate the QuickFix menu with any errors and warnings it finds
" in the log file. This is done after (attempting to) compile or pressing <Space>le. 
" This also closes the QuickFix menu when it is open.
" <Space>lt: Show a window with a table of contents for your document. Also
" shows labels, references and TODOs. Can be used to jump to sections, labels,
" references and TODOs.
" [[,[],]]: Move between section boundaries.
" ic, ac: ic referes to a LaTeX command excluding the backslash. ac includes
" the backslash. Example: cic, dac.
" :h vimtex for a full description.

"  All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" ===============
" end of plugins
" ===============

" Show the docstrings for folded code
let g:SimpylFold_docstring_preview=1
" Ignore files in NERDTree
let NERDTreeIgnore=['\.pyc$', '\~$']
map <C-f> :NERDTreeToggle<CR>
" python-syntax
let python_highlight_all=1
" Make coc.nvim automatically install the extension if it's missing
let g:coc_global_extensions=['coc-omnisharp']
let g:coc_user_config="~/.config/coc/coc-settings.json"
" To limit ALE to only use OmniSharp 
let g:ale_linters = {
\ 'cs': ['OmniSharp']
\}
" chatGPT
let g:chat_gpt_key='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'
" Set maximum number of tokens (words or characters)
" that the ChatGPT API will return in its response.
let g:chat_gpt_max_tokens=2000
" Specify the ChatGPT model 
let g:chat_gpt_model='gpt-4'
" coc.nvim
" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Support for different go to definitions for different file type
autocmd FileType cs nnoremap <silent> gd :OmniSharpGotoDefinition<CR>
autocmd FileType cs nnoremap <Leader>r :!dotnet run<CR>

syntax enable
autocmd ColorScheme * highlight LineNr ctermfg=239 guifg=#4e4e4e
colorscheme desert
set backspace=indent,eol,start
set number
set encoding=utf-8
set ruler
set noautoindent
set hlsearch
set ignorecase
set wrapscan
set incsearch
set showmatch
set nobackup
set expandtab
set wildmenu
set history=5000
" Enable folding
set foldmethod=indent
set foldlevel=99
" Enable mouse scroll
set mouse=a


" << Key command settings >>
let mapleader = "\<Space>"
nmap <Leader><Leader> V
nnoremap <CR> G
nnoremap j gj
nnoremap k gk
nnoremap <Down> gj
nnoremap <Up> gk
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)
" Tap <ESC> two times to switch highlight
nnoremap <silent><Esc><Esc> :<C-u>set nohlsearch!<CR>
" Tap <Space>w to save 
nnoremap <Leader>w :w<CR>
" Tap <Space>q to quit
nnoremap <Leader>q :q<CR>
" Tap <Space>q1 to :q!
nnoremap <Leader>q! :q!<CR>
" Tap <Space>wq to save and quit 
nnoremap <Leader>wq :wq<CR>
" Enable folding with <Space>-f
nnoremap <Leader>f za

" [[ Terminal Mode ]]
" Open terminal vertically
nnoremap tv :vert term<CR>
" Open terminal at the bottom
nnoremap tb :bo term<CR>
" Open terminal at the top
nnoremap tt :top term<CR>
" Switch to Normal Mode 
tnoremap <Esc> <C-\><C-n>

" [[ Scrolling ]]
" scroll down by one line
nnoremap <C-d> <C-e>
" scroll up by one line
nnoremap <C-u> <C-y>
" scroll down by half of the window
nnoremap <C-h><C-d> <C-d>
" scroll up by half of the window
nnoremap <C-h><C-u> <C-u>
 
" [[ Window Tab ]]
nnoremap t <Nop>
" move to next tab
nnoremap tn gt
"move to previous tab
nnoremap tp gT

" [[ Window ]]
" split horizontally
nnoremap sh :<C-u>sp<CR>
" split vertically
nnoremap sv :<C-u>vs<CR>
" move left
nnoremap mj <C-w>h
" move right
nnoremap ml <C-w>l
" move down
nnoremap m, <C-w>j
" move up
nnoremap mk <C-w>k
" move next
nnoremap mn <C-w>w
" move window left
nnoremap mJ <C-w>H
" move window right
nnoremap mL <C-w>L
" move window down
nnoremap m< <C-w>J
" move window up
nnoremap mK <C-w>K
" rotate windows
nnoremap rw <C-w>r
" same size
nnoremap w0 <C-w>=
nnoremap w= <C-w>=
" increase width by w>
call submode#enter_with('bufmove', 'n', '', 'w>', '<C-w>>')
call submode#map('bufmove', 'n', '', '>', '<C-w>>')
" decrease width by w<
call submode#enter_with('bufmove', 'n', '', 'w<', '<C-w><')
call submode#map('bufmove', 'n', '', '<', '<C-w><')
" increase height by w+
call submode#enter_with('bufmove', 'n', '', 'w+', '<C-w>+')
call submode#map('bufmove', 'n', '', '+', '<C-w>+')
" decrease height by w-
call submode#enter_with('bufmove', 'n', '', 'w-', '<C-w>-')
call submode#map('bufmove', 'n', '', '-', '<C-w>-')


" Search and Paste
"/something to search
"push cs, replace, then push <Esc>
"n to jump to the next 
". to replace
vnoremap <silent> s //e<C-r>=&selection=='exclusive'?'+1':''<CR><CR>
         \:<C-u>call histdel('search',-1)<Bar>let @/=histget('search',-1)<CR>gv
omap s :normal vs<CR>


" vp doesn't replace paste buffer
function! RestoreRegister()
  let @" = s:restore_reg
  return ''
endfunction
function! s:Repl()
  let s:restore_reg = @"
  return "p@=RestoreRegister()\<cr>"
endfunction
vmap <silent> <expr> p <sid>Repl()

" Paste without indents when pasting from clipboard
if &term =~ "xterm"
    let &t_SI .= "\e[?2004h"
    let &t_EI .= "\e[?2004l"
    let &pastetoggle = "\e[201~"

    function XTermPasteBegin(ret)
        set paste
        return a:ret
    endfunction

    inoremap <special> <expr> <Esc>[200~ XTermPasteBegin("")
endif


"""""""""""""""""""""""
" Python Configuration
"""""""""""""""""""""""
" PEP8
au BufNewFile,BufRead *.py
    \ set cursorcolumn |
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=125 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

"""""""""""""""""""""""
" C# Configuration
"""""""""""""""""""""""
au BufNewFile,BufRead *.cs
    \ set tabstop=3 |
    \ set softtabstop=3 |
    \ set shiftwidth=3

"""""""""""""""""""""""
" latex
"""""""""""""""""""""""
let g:vimtex_compiler_latexmk = {
    \ 'background': 1,
    \ 'build_dir': '',
    \ 'continuous': 1,
    \ 'options': [
    \    '-pdfdvi',
    \    '-verbose',
    \    '-file-line-error',
    \    '-synctex=1',
    \    '-interaction=nonstopmode'
    \]
    \}
let g:vimtex_view_general_viewer
    \ = '/Applications/Skim.app/Contents/SharedSupport/displayline'
let g:vimtex_view_general_options = '-r @line @pdf @tex'
let g:tex_flavor="latex"
let g:syntastic_tex_checkers=['lacheck']
let maplocalleader=' '
au BufNewFile,BufRead *.tex
    \ set spell spelllang=en_us |
    \ set tabstop=3 |
    \ set softtabstop=3 |
    \ set shiftwidth=3 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

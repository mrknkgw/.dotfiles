#!/bin/sh

# stop when there's an error
set -e

if [ "$(uname)" == "Darwin" ]; then

	if [ -f /usr/local/bin/brew ]; then
		echo "Homebrew already installed, skipping"
	else
		echo "Installing Homebrew"
		/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	fi
fi

if [ -d ~/.dotfiles ]; then
	echo ".dotfiles already exist, skipping"
else
	echo "git clone dotfiles to .dotfiles"
	cd ~
	git clone git@bitbucket.org:mrknkgw/.dotfiles.git .dotfiles
fi

if [ "$(uname)" == "Darwin" ]; then
	echo "Installing softwares listed in the Brewfile"
	brew bundle -v --file=~/.dotfiles/Brewfile

	if [ -d ~/.oh-my-zsh ]; then
		echo "Oh My Zsh already installed, skipping"
	else
		echo "Installing Oh My Zsh"
		echo "=====================IMPORTANT======================"
		echo "Make sure to exit zsh once its installation is done!"
		echo "===================================================="
		sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    echo "Installing zsh-syntax-highlighting under oh-my-zsh"
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    echo "Installing zsh-autosuggestions under oh-my-zsh"
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
	fi

  if [ -d ~/.config/nvim ]; then
    echo "Neovim already installed, skipping"
  else
    echo "Installing my neovim setup"
    git clone git@bitbucket.org:mrknkgw/nvim.git ~/.config/nvim

  if [ -d ~/.tmux/plugins/tpm ]; then
    echo "tmux plugin manager, tpm already installed, skipping"
  else
    echo "Installing tmux plugin manager, tpm"
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

  echo "Installing modular"
  curl https://get.modular.com | sh - && modular auth mut_179a2cc61e8942d1a1d6584abcd2fb7e
  echo "Installing mojo"
  modular install mojo
fi
